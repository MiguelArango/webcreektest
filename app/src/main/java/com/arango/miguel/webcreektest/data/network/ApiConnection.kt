package com.arango.miguel.webcreektest.data.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

class ApiConnection(): Callable<Api>{

    lateinit var api: Api
    lateinit var retrofit: Retrofit

    override fun call(): Api {
        return requestSyncCall()
    }

    fun connectionApi(): ApiConnection {
        return ApiConnection()
    }

    fun connectToApi() {

        val apiUrl: String = "https://secure.modinter.com.ec:8586/"
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
            .readTimeout(180000, TimeUnit.MILLISECONDS)
            .connectTimeout(180000, TimeUnit.MILLISECONDS)
            .addInterceptor(logging)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(apiUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(httpClient)
            .build()
    }

    internal fun requestSyncCall(): Api {
        connectToApi()
        api = retrofit.create(Api::class.java)
        return api
    }
}