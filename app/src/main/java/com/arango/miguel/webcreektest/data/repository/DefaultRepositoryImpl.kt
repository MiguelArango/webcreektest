package com.arango.miguel.webcreektest.data.repository

import android.util.Log.d
import android.util.Log.e
import com.arango.miguel.webcreektest.domain.callback.StartFortunesCallback
import com.arango.miguel.webcreektest.domain.callback.StopFortunesCallback
import com.arango.miguel.webcreektest.data.network.ApiImpl
import com.arango.miguel.webcreektest.domain.repository.DefaultRepository
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DefaultRepositoryImpl: DefaultRepository{

    val TAG = this.javaClass.simpleName
    val api = ApiImpl()
    val responseCall: Call<ResponseBody> = api.getFortunes()

    override fun stopFortunes(callback: StopFortunesCallback) {

    }

    override fun startFortunes(callback: StartFortunesCallback) {


        responseCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                d(TAG, "onResponse")
                if (response.isSuccessful()) {
                    d(TAG, "onResponse Successfull")
                    callback.onReceiveMessage(response.message())
                } else {
                    d(TAG, "onResponse Error")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                e(TAG, "onFailure")
            }
        })

    }



}