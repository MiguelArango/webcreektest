package com.arango.miguel.webcreektest.data.network

import okhttp3.ResponseBody
import retrofit2.Call

class ApiImpl: Api {

    override fun getFortunes(): Call<ResponseBody> {
        val api = ApiConnection().connectionApi().requestSyncCall()
        return api.getFortunes()
    }
}