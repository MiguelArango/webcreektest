package com.arango.miguel.webcreektest.ui.presenter

import com.arango.miguel.webcreektest.ui.view.BaseView


public abstract class Presenter<T : BaseView> {


    private var view: T? = null

    fun getView(): T? {
        return view
    }

    fun setView(view: T) {
        this.view = view
    }

    abstract fun initialize()

}