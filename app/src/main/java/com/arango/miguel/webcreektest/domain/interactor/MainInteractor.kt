package com.arango.miguel.webcreektest.domain.interactor

import com.arango.miguel.webcreektest.domain.repository.DefaultRepository
import com.arango.miguel.webcreektest.domain.callback.StartFortunesCallback
import com.arango.miguel.webcreektest.domain.callback.StopFortunesCallback


class MainInteractor(private val defaultRepository: DefaultRepository) {


    fun startFortunes(callback: StartFortunesCallback){
        defaultRepository.startFortunes(callback)
    }

    fun stopFortunes(callback: StopFortunesCallback){
        defaultRepository.stopFortunes(callback)
    }


}