package com.arango.miguel.webcreektest.ui.view


interface MainView : BaseView {

    fun renderMessage(message: String)

}