package com.arango.miguel.webcreektest.domain.callback

interface StartFortunesCallback {

    fun onReceiveMessage(message: String)
}