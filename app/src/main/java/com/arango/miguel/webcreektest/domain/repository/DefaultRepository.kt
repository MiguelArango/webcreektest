package com.arango.miguel.webcreektest.domain.repository

import com.arango.miguel.webcreektest.domain.callback.StartFortunesCallback
import com.arango.miguel.webcreektest.domain.callback.StopFortunesCallback

interface DefaultRepository {


    fun startFortunes(callback: StartFortunesCallback)
    fun stopFortunes(callback: StopFortunesCallback)

}