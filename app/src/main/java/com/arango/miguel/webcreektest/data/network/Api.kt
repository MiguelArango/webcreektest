package com.arango.miguel.webcreektest.data.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Streaming

interface Api {

    @Streaming
    @GET("fortunes/stream")
    fun getFortunes(): Call<ResponseBody>

}