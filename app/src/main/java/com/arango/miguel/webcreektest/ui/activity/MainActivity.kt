package com.arango.miguel.webcreektest.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.arango.miguel.webcreektest.R
import com.arango.miguel.webcreektest.ui.presenter.MainPresenter
import com.arango.miguel.webcreektest.ui.view.MainView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    View.OnClickListener,
    MainView {


    override fun renderMessage(message: String) {
        tvMessage.text = message
    }


    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.bStart -> presenter.startFotunes()
            R.id.bStop -> presenter.stopFortunes()
            R.id.bSimulate -> presenter.simulate()
        }
    }

    lateinit var presenter: MainPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()
        bStart.setOnClickListener(this)
        bStop.setOnClickListener(this)
        bSimulate.setOnClickListener(this)
    }

    fun initPresenter(){
        presenter = MainPresenter()
        presenter.setView(this)
        presenter.initialize()
    }

}
