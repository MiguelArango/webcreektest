package com.arango.miguel.webcreektest.ui.presenter

import com.arango.miguel.webcreektest.data.repository.DefaultRepositoryImpl
import com.arango.miguel.webcreektest.domain.callback.StartFortunesCallback
import com.arango.miguel.webcreektest.domain.callback.StopFortunesCallback
import com.arango.miguel.webcreektest.domain.interactor.MainInteractor
import com.arango.miguel.webcreektest.ui.view.MainView


class MainPresenter: Presenter<MainView>(),
    StartFortunesCallback,
    StopFortunesCallback {


    override fun onReceiveMessage(message: String) {
        getView()?.renderMessage(message)
    }

    lateinit var interactor: MainInteractor

    override fun initialize() {
        interactor = MainInteractor(DefaultRepositoryImpl())
    }

    fun startFotunes(){
        interactor.startFortunes(this)
    }




    fun stopFortunes(){
        interactor.stopFortunes(this)
    }

    override fun onStopFailed() {

    }

    override fun onStopSuccess() {

    }


    fun simulate(){

    }
}