package com.arango.miguel.webcreektest.domain.callback

interface StopFortunesCallback {

    fun onStopSuccess()
    fun onStopFailed()
}